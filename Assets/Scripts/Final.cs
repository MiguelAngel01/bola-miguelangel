﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Final : MonoBehaviour
{
    public Text puntos;

        // Start is called before the first frame update
    void Start()
    {
        puntos.text = "Puntuación final: " + GameData.puntuacion;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void ClickMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
