﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameScene : MonoBehaviour
{

    public Text puntos;
	public Text muerte;
    
    // Start is called before the first frame update
    void Start()
    {
        GameData.puntuacion = 0;
		GameData.muertes = 0;
    }

    // Update is called once per frame
    void Update()
    {
        puntos.text = "Puntuación: " + GameData.puntuacion;
		muerte.text = "Muertes: " + GameData.muertes;
    }
}
