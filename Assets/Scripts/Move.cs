using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Move : MonoBehaviour
{

    public float speed;
    private Rigidbody rigidbody;
    public float forceValue;
    public float jumpValue;
    private AudioSource audio;
    
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && Mathf.Abs(rigidbody.velocity.y) < 0.01f ) {
            rigidbody.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
            audio.Play();
        }
        rigidbody.AddForce(new Vector3(Input.acceleration.x, 0, Input.acceleration.y) * forceValue);
        
    }
    void FixedUpdate()
    {
        rigidbody.AddForce(new Vector3(Input.GetAxis("Horizontal"),
                            0,
                            Input.GetAxis("Vertical")) * forceValue);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            print("La esfera ha muerto");
            transform.position = new Vector3(0, 0, 0);
            GameData.muertes++;
        } else if (collision.gameObject.tag == "EnemigoEliminable")
        {
            Destroy(collision.gameObject);
            GameData.puntuacion += 2;
        } else if (collision.gameObject.tag == "Puntos")
        {
            GameData.puntuacion++;
            Destroy(collision.gameObject);
        } else if (collision.gameObject.tag == "Final")
        {
            SceneManager.LoadScene("Final");
        } else if (collision.gameObject.tag == "Suelo")
        {
            transform.position = new Vector3(0, 0, 0);
            GameData.muertes++;
        }
        
    }
    
    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Plataforma")
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.angularDrag = 0.05F;
        }
    }
    
}
